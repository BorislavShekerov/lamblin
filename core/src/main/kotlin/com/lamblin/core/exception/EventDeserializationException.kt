package com.lamblin.core.exception

class EventDeserializationException(message: String, e: Exception) : RuntimeException(message, e)
